.. jones_solvers documentation master file, created by
   sphinx-quickstart on Wed Jul 13 10:40:20 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Jones Solver Library
====================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   installation
   usage
   performance
   api
   api/modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

