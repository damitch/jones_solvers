Installation
############

Once |rcl| has been `installed using pip <https://ska-telescope.gitlab.io/external/rascil/RASCIL_install.html>`_,
clone and install the jones_solvers package as follows.

.. code-block:: shell

 git clone https://gitlab.com/damitch/jones_solvers.git
 cd jones_solvers
 pip install .

Unit tests can be run to check the installation and dependencies:

.. code-block:: shell

 python tests/processing_components/test_solve_jones.py

Example driver scripts are also available:

.. code-block:: shell

 python examples/scripts/run_jones_solvers.py
 python examples/scripts/run_AA0.5.py

Dependencies
============

* Python 3.8 or greater (required for |rcl|)
* Radio Astronomy Simulation, Calibration and Imaging Library (|rcl|) and its dependencies

