#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import glob
from distutils.sysconfig import get_config_vars

from setuptools import setup
import setuptools

assert sys.version_info[0] >= 3

with open("README.md") as readme_file:
    readme = readme_file.read()

version = {}
VERSION_PATH = os.path.join("jones_solvers", "version.py")
with open(VERSION_PATH, "r") as file:
    exec(file.read(), version)

# # List of requirements cannot contain lines that start with #
# # can neither work with git+https ones, hence we'll remove it
# # and add it back below with the correct syntax for setup.py
# reqs = [
#     line.strip()
#     for line in open("requirements.txt").readlines()
#     if not line.strip().startswith("#")
#     and line.strip() != ""
#     and not line.strip().startswith("--extra-index-url")
# ]

# NB. These are not really Python extensions (i.e., they do not
# Py_Initialize() and they do define main() ), we are just cheating to
# re-use the setuptools build support.

packages = ["jones_solvers"]
packages_data = [
    i
    for p in packages
    for i in glob.glob(p + "/*/")
    + glob.glob(p + "/*/*/")
    + glob.glob(p + "/*/*/*/")
]

setup(
    name="jones_solvers",
    version=version["__version__"],
    python_requires=">=3.0",
    description="Jones Solver Library",
    long_description="A reference library of python-based antenna Jones matrix solvers",
    author="Daniel Mitchell",
    author_email="daniel.mitchell@csiro.au",
    #url="https://gitlab.com/ska-telescope/...",
    license="UWA and CSIRO Open Source Software Licence Agreement",
    zip_safe=False,
    classifiers=[
        "Development Status :: Beta",
        "Intended Audience :: Developers",
        "License :: variation of the BSD License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
    ],
    packages=(packages + packages_data),
    # install_requires=reqs,
    #scripts=["bin/get_rascil_data"],
    test_suite="tests",
    tests_require=["pytest"],
)

